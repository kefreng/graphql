package cl.gama.gamagraphql.model;

public class AuthorWrapper {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
