package cl.gama.gamagraphql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GamagraphqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(GamagraphqlApplication.class, args);
	}

}
