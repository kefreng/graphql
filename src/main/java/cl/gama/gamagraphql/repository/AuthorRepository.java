package cl.gama.gamagraphql.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.gama.gamagraphql.model.Author;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Integer> {

}
