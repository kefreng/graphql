package cl.gama.gamagraphql.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.gama.gamagraphql.model.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, String> {

}
